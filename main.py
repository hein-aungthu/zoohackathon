
import re
import requests
from datetime import date
from lxml import html, etree
from google.cloud import bigquery
import pdb
import random 
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from google.cloud.bigquery.client import Client
from google.oauth2 import service_account

def post_id():
    postID = random.randint(0,10000000000000000)
    return postID

#getting data from googlesheets
def get_keys_from_sheets():
    scope = [
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.file'
    ]
    file_name = 'keysheet.json'
    credentials = ServiceAccountCredentials.from_json_keyfile_name(file_name,scope)
    client = gspread.authorize(credentials)
    sheet = client.open('keywords').sheet1
    col = sheet.col_values(1)
    return(col)


def fetch_url(url):
    headers_Get = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate',
            'DNT': '1',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1'
    }
    return requests.get(url, headers=headers_Get)

#Scrape data from google search engine
def link_scraper():
    keywords =  get_keys_from_sheets()
    link_list = []
    for keyword in keywords:
        response = fetch_url('https://www.google.com/search?q={}'.format(keyword))
        raw = response.text
        tree = html.fromstring(raw)
        els = tree.xpath('//div[@class="rc"]//div[@class="yuRUbf"]')
        for el in els:
            link_list.append(el.find('a').attrib['href'])
        return link_list

#open each sites and count its keyword content
def key_word_counts():
    keywords = get_keys_from_sheets()
    data =  link_scraper()
    keyword_counts = []
    for data_gathered in data:
        url = fetch_url(data_gathered)
        for keys in keywords:
            search = re.findall('{}'.format(keys), url.text)
            if (len(search) != 0):
                keyword_counts.append(len(search))
    return keyword_counts

#get used keywords
def used_keywords():
    keywords = get_keys_from_sheets()
    data =  link_scraper()
    keyword_used = []
    for data_gathered in data:
        url = fetch_url(data_gathered)
        for keys in keywords:
            search = re.findall('{}'.format(keys), url.text)
            if (len(search) != 0):
                keyword_used.append(keys)
    print(keyword_used)
    return keyword_used
used_keywords()

#def get_data():
#    keywords = used_keywords()
#    keyword_counts = key_word_counts()
#    website_links = link_scraper()
#    dates = date.today()
#    Website_postId = post_id()
#    print(keywords, keyword_counts, website_links, dates, Website_postId)
#get_data()

'''
    # Construct a BigQuery client object.
    client = Client.from_service_account_json('key.json')

    # TODO(developer): Set table_id to the ID of table to append to.
    table_id = "zoohackathon-294911.zoohackathon_datasets.zoohackathon-table"

    rows_to_insert = [
        {"zoohackathon_websites": website_links,
         "zoohackathon_keywords": keywords,
         "zoohackathon_keyword_counts": keyword_counts,
         "zoohackathon_date":dates.strftime('%y/%m/%d')
        }
    ]
    errors = client.insert_rows_json(table_id, rows_to_insert)  # Make an API request.
    if errors == []:
        print("New rows have been added.")
    else:
        print("Encountered errors while inserting rows: {}".format(errors))
    
'''



